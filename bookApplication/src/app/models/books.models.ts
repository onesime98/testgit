export class Books {
  photo: string;
  synopsis: string;

  constructor(public title: string, public author: string) {
  }
}
