import {Component} from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor() {
    // Your web app's Firebase configuration
    var firebaseConfig = {
      apiKey: 'AIzaSyAvqeyHZpN3vmyPhGzPDw26lbVIgTQPi_g',
      authDomain: 'bookapplication-830d4.firebaseapp.com',
      databaseURL: 'https://bookapplication-830d4.firebaseio.com',
      projectId: 'bookapplication-830d4',
      storageBucket: 'gs://bookapplication-830d4.appspot.com',
      messagingSenderId: '519898258421',
      appId: '1:519898258421:web:813453b8bb2762b8'
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  }
}
