import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SignupComponent} from './authentification/signup/signup.component';
import {SigninComponent} from './authentification/signin/signin.component';
import {BookListComponent} from './book-list/book-list.component';
import {SingleBookComponent} from './book-list/single-book/single-book.component';
import {BookFormComponent} from './book-list/book-form/book-form.component';
import {HeaderComponent} from './header/header.component';
import {AuthentificationGuardService} from './services/authentification-guard.service';
import {BooksService} from './services/books.service';
import {AuthentificationService} from './services/authentification.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

const appRoutes: Routes = [
  {path: 'authentification/signup', component: SignupComponent},
  {path: 'authentification/signin', component: SigninComponent},
  {path: 'books', canActivate: [AuthentificationGuardService], component: BookListComponent},
  {path: 'books/new', canActivate: [AuthentificationGuardService], component: BookFormComponent},
  {path: 'books/view/:id', canActivate: [AuthentificationGuardService], component: SingleBookComponent},
  {path: '', redirectTo: 'books', pathMatch: 'full'},
  {path: '**', redirectTo: 'books'}
];

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    BookListComponent,
    SingleBookComponent,
    BookFormComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthentificationGuardService,
    BooksService,
    AuthentificationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
