import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {Observable} from 'rxjs';
import * as firebase from 'firebase';
@Injectable()
export class AuthentificationGuardService implements CanActivate {

  constructor(private route: Router) {
  }

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise((resolve, reject) => {
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          resolve(true);
        } else {
          this.route.navigate(['/authentification', 'signin']);
          resolve(false);
        }
      });
    });
  }
}
