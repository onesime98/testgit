import { Component, OnInit } from '@angular/core';
import {AuthentificationService} from '../../services/authentification.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  errorMessage: string;
  constructor(private authentificationService: AuthentificationService, private route: Router) {
  }

  ngOnInit() {
  }
  onSubmit(form: NgForm) {
    const email = form.value['email'];
    const password = form.value['password'];
    this.authentificationService.signInUser(email, password).then(
      () => {
        this.route.navigate(['/books']);
      },
      (error) => {
        this.errorMessage = error;

      }
    );

  }
}
