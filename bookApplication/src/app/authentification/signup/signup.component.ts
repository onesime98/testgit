import {Component, OnInit} from '@angular/core';
import {AuthentificationService} from '../../services/authentification.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  errorMessage: string;

  constructor(private authentificationService: AuthentificationService, private route: Router) {
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    const email = form.value['email'];
    const password = form.value['password'];
    this.authentificationService.createNewUser(email, password).then(
      () => {
        this.route.navigate(['authentification/signin']);
      },
      (error) => {
        this.errorMessage = error;

      }
    );

  }


}
