import {Component, OnDestroy, OnInit} from '@angular/core';
import {Books} from '../models/books.models';
import {Subscription} from 'rxjs';
import {BooksService} from '../services/books.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit, OnDestroy {
  books: Books[];
  booksSubscription: Subscription;

  constructor(private bookService: BooksService, private route: Router) {
  }

  ngOnInit() {
    this.booksSubscription = this.bookService.booksSubject.subscribe(
      (books: Books[]) => {
      this.books = books;
    });
    this.bookService.getBooks();
    this.bookService.emitBooks();
  }

  onNewBook() {
    this.route.navigate(['/books', 'new']);
  }

  onDeleteBook(books: Books) {
    this.bookService.removeBook(books);
  }

  onViewBook(id: number) {
    this.route.navigate(['/books', 'view', id]);

  }

  ngOnDestroy(): void {
    this.booksSubscription.unsubscribe();
  }


}
