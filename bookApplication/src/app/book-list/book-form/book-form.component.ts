import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Books} from '../../models/books.models';
import {Router} from '@angular/router';
import {BooksService} from '../../services/books.service';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss']
})
export class BookFormComponent implements OnInit {
  fileIsUploading = false;
  fileUrl: string;
  fileUploaded = false;

  constructor(private bookService: BooksService, private router: Router) {
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    const titre = form.value['titre'];
    const password = form.value['author'];
    const synopsis = form.value['synopsis'];
    const newBook = new Books(titre, password);
    newBook.synopsis = synopsis;
    if (this.fileUrl && this.fileUrl !== '') {
      newBook.photo = this.fileUrl;
    }
    this.bookService.createNewBook(newBook);
    this.router.navigate(['/books']);
  }

  onUploadFile(file: File) {
    this.fileIsUploading = true;
    this.bookService.uploadFile(file).then(
      (url: string) => {
        this.fileUrl = url;
        this.fileIsUploading = false;
        this.fileUploaded = true;
      }
    );
  }

  detectFiles(event) {
    this.onUploadFile(event.target.files[0]);
  }
}
